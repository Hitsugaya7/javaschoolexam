package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y)  {
        // TODO: Implement the logic here
        if(x == null || y == null){
                throw new IllegalArgumentException("error");

        }
            int indexOf=0;
            int indexOf2=0;
            for (int i = 0; i < x.size(); i++) {
                if(i==0){
                    if(y.contains(x.get(i))){
                        indexOf = y.indexOf(x.get(i));
                    }
                    else return false;
                }
                else {
                    List list = (y.subList(indexOf+1+indexOf2, y.size()));
                    if(list.contains(x.get(i))){
                        indexOf2+=list.indexOf(x.get(i))+1;
                    }
                    else return false;
                }
            }
            return true;

    }
}
