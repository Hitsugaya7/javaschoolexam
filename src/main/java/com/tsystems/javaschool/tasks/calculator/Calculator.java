package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Stack;

public class Calculator {
    private LinkedHashMap<Character, Integer> map;
    private Stack<Character> stack;
    private ArrayList list;
    private StringBuilder stringBuilder;
    private Stack<String> st;

    Calculator(){
        map = new LinkedHashMap<>();
        stack = new Stack<>();
        list = new ArrayList<>();
        st = new Stack();

    }



    private void MapPut() {

        map.put('*', 3);
        map.put('/', 3);
        map.put('+', 2);
        map.put('-', 2);
        map.put('(', 1);
    }

    private ArrayList ReversePolishEntry(String s) {
        try {
            if(s.isEmpty())
                return null;
            for (int i = 0; i < s.length(); i++) {
                char temp = s.charAt(i);
                if (!Character.isDigit(temp) && temp != '.') {
                    if (temp == ')') {
                        while (stack.lastElement() != '(') {
                            list.add(stack.pop());

                        }
                        stack.pop();
                    } else if (temp == '(') {
                        stack.push(temp);
                    } else if (stack.empty() || (map.get(stack.lastElement()) < map.get(temp))) {
                        stack.push(temp);
                    } else {
                        do {
                            list.add(stack.lastElement());
                            stack.pop();
                        } while (!(stack.empty() || (map.get(stack.lastElement()) < map.get(temp))));
                        stack.push(temp);
                    }
                } else {
                    int index = i;
                    stringBuilder = new StringBuilder("");
                    while (Character.isDigit(temp) || temp == '.') {
                        stringBuilder.append(temp);
                        if (index != s.length() - 1) {
                            index++;
                            temp = s.charAt(index);
                        } else break;
                    }

                    if (index == s.length() - 1) {
                        i = ++index;
                    }
                    i = index - 1;
                    list.add(stringBuilder);
                }
            }
            while (!stack.empty())
            {
                list.add(stack.pop());
            }

        } catch (Exception e) {
            return null;
        }
        return list;
    }

    private BigDecimal Result(ArrayList list) {
        BigDecimal decimal;
        try {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).equals('*') || list.get(i).equals('/') || list.get(i).equals('-') || list.get(i).equals('+')) {
                    switch ((char) list.get(i)) {
                        case '+':
                            st.push(String.valueOf(new BigDecimal(st.pop()).add(new BigDecimal(st.pop()))));
                            break;
                        case '-':
                            decimal = new BigDecimal(st.pop());
                            st.push(String.valueOf(new BigDecimal(st.pop()).subtract(decimal)));
                            break;
                        case '*':
                            st.push(String.valueOf(new BigDecimal(st.pop()).multiply(new BigDecimal(st.pop()))));
                            break;
                        case '/':
                            decimal = new BigDecimal(st.pop());
                            st.push(String.valueOf(new BigDecimal(st.pop()).divide(decimal,8,BigDecimal.ROUND_CEILING)));
                            break;
                    }
                } else {

                    st.push(new String((StringBuilder) list.get(i)));
                }
            }

        } catch (Exception e) {
            return null;
        }
        return new BigDecimal(st.pop()).setScale(4, BigDecimal.ROUND_DOWN);
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        MapPut();
        BigDecimal result = Result(ReversePolishEntry(statement));

        if (result == null) {
            return null;
        }
        if ((result != null ? result.doubleValue() : 0) % 1 == 0) {
            return String.valueOf(result.intValue());
        } else return String.valueOf(result.doubleValue());


    }

}
